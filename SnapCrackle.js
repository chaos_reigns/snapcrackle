const maxValue = 13
function snapCrackle() {
    
    
    var finalString = '';
    for (let i = 1; i <= maxValue; i++){
        if (i % 2 !== 0){
            finalString += "Snap, ";
            }else if(i % 5 === 0){
                finalString += "Crackle, ";
            }else if(i % 2 !== 0 || i % 5 === 0){
                finalString += "SnapCrackle, ";
            }else{
                finalString += i + ", ";
            }
    }
    return(finalString)
}